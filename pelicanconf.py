#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'William Salmon'
SITENAME = 'Points Waves'
#SITEURL = 'pointswaves.gitlab.io/blog'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/London'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

PATH = 'content'
STATIC_PATHS = ['media', ]

from distutils.sysconfig import get_python_lib
PLUGIN_PATHS = [get_python_lib()]

PLUGINS = [
            'pelican_image_process',
          ]
IMAGE_PROCESS_PARSER = "html5lib"
#IMAGE_PROCESS = {
#    'thumb': ["crop 0 0 50% 50%", "scale_out 150 150 True", "crop 0 0 150 150"],
#    }

IMAGE_PROCESS = {
  'article-image': ["scale_in 300 300 True"],
  'large-photo': {'type': 'responsive-image',
        'sizes': '(min-width: 1200px) 800px, (min-width: 992px) 650px, \
                (min-width: 768px) 718px, 100vw',
        'srcset': [('600w', ["scale_in 600 450 True"]),
                ('800w', ["scale_in 800 600 True"]),
                ('1600w', ["scale_in 1600 1200 True"]),
                ],
        'default': '800w',
        },           
  }    

# Blogroll
#LINKS = (('Pelican', 'http://getpelican.com/'),
#         ('Python.org', 'http://python.org/'),
#         ('Jinja2', 'http://jinja.pocoo.org/'),
#         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('gitlab', 'https://www.gitlab.com/pointswaves'),
        ('github', 'https://www.github.com/fishrockz'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

THEME = './pelican-alchemy/alchemy'
