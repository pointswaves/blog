Why does git not work in buildbox
=================================

:date: 20220423 08:30
:tags: build
:category: rust
:slug: buildbox-git
:author: William Salmon
:summary: run git in buildbox

Preface
-------

git does not run in buildbox

.. code-block:: shell

	$ strace -f git clone https://gitlab.com/pointswaves/bit-byte-structures > stracegood ?> stracegood

Please see `strace good <https://pointswaves.gitlab.io/blog/media/strace/stracegood>`_

Then when run in buildbox this fails

Please see `strace bad <https://pointswaves.gitlab.io/blog/media/strace/stracebad2>`_


good

.. code-block:: shell

	[pid 221718] openat(AT_FDCWD, "/home/will/projects/git_test/bit-byte-structures/.git/objects/pack/tmp_pack_wBnaP6", O_RDWR|O_CREAT|O_EXCL, 0444) = 3
	...
	[pid 221718] openat(AT_FDCWD, "/home/will/projects/git_test/bit-byte-structures/.git/objects/pack", O_RDONLY|O_NONBLOCK|O_CLOEXEC|O_DIRECTORY <unfinished ...>
	[pid 221718] <... openat resumed>)      = 4
	[pid 221718] newfstatat(4, "",  <unfinished ...>
	[pid 221718] <... newfstatat resumed>{st_mode=S_IFDIR|0775, st_size=30, ...}, AT_EMPTY_PATH) = 0
	[pid 221718] getdents64(4,  <unfinished ...>
	[pid 221718] <... getdents64 resumed>0x55f1a7f84110 /* 3 entries */, 32768) = 88
	[pid 221718] getdents64(4,  <unfinished ...>
	[pid 221718] <... getdents64 resumed>0x55f1a7f84110 /* 0 entries */, 32768) = 0
	[pid 221718] close(4 <unfinished ...>
	[pid 221718] <... close resumed>)       = 0
	...
	[pid 221718] write(3, "PACK\0\0\0\2\0\0\0000\225\20x\234\235\314=n\304 \20@\341\236SL\37i\0053"..., 4096 <unfinished ...>
	...
	[pid 221718] <... write resumed>)       = 4096

bad, note that like good, git seems to mess with the directory that has already been created between opening and writing to the file.

.. code-block:: shell

	mkdir("/src/bit-byte-structures/.git/objects/pack", 0777) = 0
	...
	[pid    18] openat(AT_FDCWD, "/src/bit-byte-structures/.git/objects/pack/tmp_pack_jwEXRX", O_RDWR|O_CREAT|O_EXCL, 0444) = 3
	...
	[pid    18] openat(AT_FDCWD, "/src/bit-byte-structures/.git/objects/pack", O_RDONLY|O_NONBLOCK|O_CLOEXEC|O_DIRECTORY) = 6
	[pid    18] fstat(6, {st_mode=S_IFDIR|0775, st_size=0, ...}) = 0
	[pid    18] getdents64(6, 0x55b8ad6c71c0 /* 1 entries */, 32768) = 40
	[pid    18] getdents64(6, 0x55b8ad6c71c0 /* 0 entries */, 32768) = 0
	[pid    18] close(6)                    = 0
	...
	[pid    18] write(3, "PACK\0\0\0\2\0\0\0000\225\20x\234\235\314=n\304 \20@\341\236SL\37i\0053"..., 4096) = -1 EIO (Input/output error)


Note that good was on a more modden glibc. the buildbox chroot is debian stable and taken from a chroot, git works when in the chroot rather than
in the buildbox environment.