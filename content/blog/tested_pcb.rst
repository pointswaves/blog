Pcb Testing
===========

:date: 20200804 08:30
:tags: PointBlank, PCB, electronics
:category: PointBlank
:slug: pcb-created
:author: William Salmon
:summary: Pointblank pcb Populated




I populated the board soon after it arrived but i never got round to writting it up in my blog


.. image:: media/electronics/2018Dec_pcb_test.png
   :width: 200pt
   :alt: pcb working


This image is a still from https://photos.app.goo.gl/7Di1HE7H9Ss9qzDD9 and shows both the low power and high power channels working well.

I never tried out this board with point blank as we have not done a lot with point blank since I created this board and I have been busy with many other things but I thought I should finish this set of blog posts off.


Robot wars robots are really quite complex and can end up consuming a huge amount of time but working on point blank taught me a lot and while I am happy to work on other things now I am really glad to have had the opportunity to have worked on it.
