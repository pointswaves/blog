Woodworking in 2020
===================

:date: 20201026 08:30
:tags: Woodwork, House
:category: Woodwork
:slug: 2020-woodwork
:author: William Salmon
:summary: Woodworking over 2020



Intro
-----

This has been a crazy year, Deborah and I now have our own house while we have had a lot of problems
I have taken advantage of this to do some thing I have been looking forward to for a very long time.
I have had the small box room in the last 4 house i have live in as a little office but have always wanted
to have desks that really make the best use of the space.

I have previously build little multi level corner tables to better make use of the space

But now that we own the house I can make it the way I want! well, time and money permitting!

It would have been great to make a hard wood table top and have it all sorted already

But while I have made do with a softwood table top I am really pleased with it and while it is not complete it is already
going to be really useful.

I am going to put the development on hold for a little while some other jobs have.

The Journey
-----------

The workbench
^^^^^^^^^^^^^

One of the first and most useful things I built was a work bench.

.. image:: media/woodwork/IMG_20200717_162355.jpg  
   :width: 200pt
   :alt: woodwork
   :class: image-process-large-photo

.. image:: media/woodwork/IMG_20200717_162350.jpg  
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image


It was quite flimsy until I added the sides but even thought the ply
sides are only 3mm thick they act as a effective sheer web and make the 
table pretty stiff. the wheels still have a little give but the table has
proved a effective worktop for planning, sawing and much more.

.. image:: media/woodwork/IMG_20200712_152556.jpg  
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

.. image:: media/woodwork/IMG_20200717_164856.jpg  
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

In time i hope to make a even more useful table garage what will not be on 
wheels but i think even then having a table that can be wheeled out will still
prove very useful. it is so much nice to paint or oil outside than inside.

.. image:: media/woodwork/IMG_20200712_142255.jpg
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

.. image:: media/woodwork/IMG_20200712_142255.jpg
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

.. image:: media/woodwork/IMG_20200711_165901.jpg
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

.. image:: media/woodwork/IMG_20200712_152549.jpg  
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

.. image:: media/woodwork/IMG_20200904_160016.jpg  
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

.. image:: media/woodwork/IMG_20200712_142255.jpg
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

Extraction
^^^^^^^^^^

Another thing i am really pleased to have sorted out is dust extraction
I only have a  basic set up but its soooo much better than nothing!

.. image:: media/woodwork/IMG_20200904_115541.jpg  
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

.. image:: media/woodwork/IMG_20200924_174834.jpg  
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

My first attempt used a thick hose meant for pumping water, it was easy to great
and cheap and i thought i was a good idea, it was not. Upgrading to them
proper hose made things Sooo much better.

.. image:: media/woodwork/IMG_20200920_184311.jpg  
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

.. image:: media/woodwork/IMG_20200924_175035.jpg  
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

I used:

 - `Bucket <https://www.amazon.co.uk/gp/product/B07JGVSWND>`_
 - `Hose <https://www.amazon.co.uk/gp/product/B000RA0LOI>`_
 - `Adapter <https://www.amazon.co.uk/gp/product/B071GLN49P>`_
 - `Hover <https://www.diy.com/departments/mac-allister-corded-wet-dry-vacuum-20l-mwdv30l/3663602798149_BQ.prd>`_
 - `Cyclone <https://www.amazon.co.uk/gp/product/B07BSDY3PW>`_


I tried and gave up on:

 - `Thin bucket <https://www.amazon.co.uk/gp/product/B00ECVX1NQ>`_
 - `Thick hose <https://www.screwfix.com/p/reinforced-suction-delivery-hose-green-10m-x-1/10105>`_


Prototyping
^^^^^^^^^^^

Once I had built the table I spent some time making a test pieces to practice making
a panel from several planks.

.. image:: media/woodwork/IMG_20200718_153135.jpg  
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

.. image:: media/woodwork/IMG_20200718_153151.jpg  
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

.. image:: media/woodwork/IMG_20200717_173838.jpg  
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

I bought my first plane and started my journey of learning and practicing using them.
I still do not think of my self as remotely competent with them but I am already finding them
very useful.

.. image:: media/woodwork/IMG_20200721_171022.jpg  
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

Trying out danish oils

.. image:: media/woodwork/IMG_20200906_173731.jpg  
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

I also bought a router, There are many to chose from and this one is a bit
beast, its not supper easy to do delicate things and I wonder if I will
also end up buying a smaller one, but so long as you can effectively camp
and or create a jig this router is amazing

.. image:: media/woodwork/IMG_20200912_150653.jpg  
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

I tried out some different edges

.. image:: media/woodwork/IMG_20200912_150658.jpg  
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

And some ideas for making holes for cables, this actually makes a
pretty fair handle.

.. image:: media/woodwork/IMG_20200912_172033.jpg  
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image



Becca's desk
------------

I had been talking about making a desk quite a lot at this point,
I had spend a lot of time looking for wood and not being able to find the right
thing. At this point before i could start on my desk becca asked for
if I would help her with a desk for her house.

.. image:: media/woodwork/IMG_20200830_135312.jpg  
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

.. image:: media/woodwork/IMG_20200914_113033.jpg  
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

I helped her and we build some table tops and I will helper her turn them in to a
desk soon. But she was unable to work on her desk for a bit so I took advantage and started on mine!

.. image:: media/woodwork/IMG_20200830_141218.jpg  
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

.. image:: media/woodwork/IMG_20200830_141221.jpg  
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

The first table top
-------------------

A car full of wood!
^^^^^^^^^^^^^^^^^^^

I some what miss judged how much wood my car could easily
I knew that i could fit 2.4m planks from the foot well to the boot
but i thought i could fit more wood in the foot well than would fit
so after much trying a nice man suggested that it might fit if placed
on the dash board.

I had a very nervous drive back home with the wood on the dash.

.. image:: media/woodwork/IMG_20200904_104653.jpg  
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

.. image:: media/woodwork/IMG_20200904_104648.jpg  
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

.. image:: media/woodwork/IMG_20200904_104658.jpg  
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

The planks I bought had not been planed as well as Becca's
so planing them to a good state with the hand plane was gonna
take wayyyyy to long so I invested in a electric plane.

I'm not sure if I should have got a jointer and a thicknesser but
I decided not too mainly on cost grounds but they would have saved
me a huge amount of time and effort.

.. image:: media/woodwork/IMG_20200924_174840.jpg  
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

I bought enough wood for both sides of the box room plus a little spare and
I have planned enough for two sides.

I have glued up the first table top but i have not yet glued up the second.

The first table top
^^^^^^^^^^^^^^^^^^^

.. image:: media/woodwork/IMG_20200927_112559.jpg  
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

Although the moveable table top is very helpful, when planing ether the planks
or the made up panel i found it much easer with the trestle tables, using
3 and with them weighted down:

  * They can support the full length and with a 3rd to support the 3rd
  * Once weighted they are pretty sturdy
  * Good height


Gluing up!

I did some practice glue ups, (I clamped as if I had glued but did not use
any glue) It was a lot of time and effort to do this but Im glad I did
as it let me see the advantages and pit falls of different ways of using the clamps.
THe table top is quite big and heavy, I can lift it ok but it is a difficult shape.
so managing the weight is a important consideration

.. image:: media/woodwork/IMG_20200909_214257.jpg  
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

.. image:: media/woodwork/IMG_20200912_143141.jpg  
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

.. image:: media/woodwork/IMG_20200912_143149.jpg  
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

The actual glue up!

.. image:: media/woodwork/IMG_20200925_121856.jpg
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

Despite my best efforts there were some imperfections in the panel that
I reduced further with the electric and hand planes

.. image:: media/woodwork/IMG_20200924_174840.jpg  
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

.. image:: media/woodwork/IMG_20200927_112620.jpg  
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

The room the table is going into has a very odd curved edge to the window so I
decided to try and make the desk sort of match, the wall is just silly 
so I don't think there are any good answers here but I quite like these.

These were my first free hand routing pieces.

.. image:: media/woodwork/IMG_20200914_145948.jpg  
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image


After staining with "natural danish oils" the wood does not seem to change colour
but when compared to some untreated wood you can really see how it brings out the grain

.. image:: media/woodwork/IMG_20201024_175907.jpg
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

.. image:: media/woodwork/IMG_20201024_175926.jpg
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

The supports/frame
^^^^^^^^^^^^^^^^^^

I had a lot of ideas about this and im not sure I came up with a great solution
but so far it seems ok.

One of the test pieces I made was for trying to think about how to attach the table top
to the supporting braces.

.. image:: media/woodwork/IMG_20200927_144146.jpg  
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

.. image:: media/woodwork/IMG_20200927_144337.jpg  
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

.. image:: media/woodwork/IMG_20200927_144532.jpg  
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

One of the most fun and also tricky bits of the hole build is the one
leg i have ended up with. I was going to try for the hole desk to be supported
by the walls but i decided that having one leg would be a good compromise.

The joint at the top of the leg is quite odd and I don't know if it was a good idea
time will tell.

.. image:: media/woodwork/IMG_20201004_114303.jpg  
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

.. image:: media/woodwork/IMG_20201004_114313.jpg  
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

Wall mounting
^^^^^^^^^^^^^

One of my first DIY purchases was a power drill and some masonry bits, I have had them a while
even though your not supposed to drill holes in rental properties, i did and the cheap and 
cheerful Aldi drill bits were fine. (ish) for putting up the odd hook. but they proved to be pretty
bad at this job. as you can see the hard lugs at the top quickly broke off
from several of the bits and made life difficult.

Swapping to some nice bosh bits made life so much easier

.. image:: media/woodwork/IMG_20201005_180959.jpg  
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

.. image:: media/woodwork/IMG_20201005_181004.jpg  
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

That said a single bosh bit cost about the same as the Aldi multi pack
and I only bought bosh bits in sizes I knew I needed, I did get a bnq no
name multi pack that I think will be better than the Aldi but not as good
as the bosh bits.

Some pics of the frame coming together

.. image:: media/woodwork/IMG_20201006_172735.jpg  
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image


.. image:: media/woodwork/IMG_20201006_172902.jpg  
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

.. image:: media/woodwork/IMG_20201009_182547.jpg  
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

.. image:: media/woodwork/IMG_20201021_185406.jpg
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

.. image:: media/woodwork/IMG_20201021_185410.jpg
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

.. image:: media/woodwork/IMG_20201021_185415.jpg
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

.. image:: media/woodwork/IMG_20201021_185443.jpg
   :width: 600pt
   :alt: woodwork
   :class: image-process-article-image

Putting it all together
-----------------------

As it can be quite tricky to plane right up to the edge of a piece I make the table
top a little over sized

Once the frame was complete I had to cut the table top to fit the gap.

.. image:: media/woodwork/IMG_20201025_172919.jpg
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

At first glance this looks quite good but.. The room is not quite
square so the top did not really match.

.. image:: media/woodwork/IMG_20201025_175031.jpg
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

.. image:: media/woodwork/IMG_20201026_161150.jpg
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

With some careful planing and sawing I was able to get the table to fit
quite well, but as you can see the room has some interesting bulges
especially near the silly window edges.

.. image:: media/woodwork/IMG_20201026_170109.jpg
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

but eventually I got it more than good enough

.. image:: media/woodwork/IMG_20201026_173532.jpg
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

.. image:: media/woodwork/IMG_20201026_173542.jpg
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

.. image:: media/woodwork/IMG_20201026_173546.jpg
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

.. image:: media/woodwork/IMG_20201026_174506.jpg
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image

It can even hold my weight with out any flex even while it is not being
attached to the frame!

.. image:: media/woodwork/IMG_20201026_173853.jpg
   :width: 200pt
   :alt: woodwork
   :class: image-process-article-image




So far so Good
--------------

The room is a long way from finished, but other things in the house that are more pressing
are calling, and i really want to start using this room.

The room was full of computer and electrical things but was not a useful space and
was not storing nearly as much stuff as it should have been.

I was hopping that by now I would have finished both tops to a much more polished and useful
state but currently the room already can hold a lot of things while also being
much more useful than it was. so while I would really like to finish both tops to
and add some nice polish. I need to take what I have and make the most of it.

But not leave too long before I keep going on this project.


.. image:: media/woodwork/IMG_20201026_174523.jpg
   :width: 600pt
   :alt: woodwork


.. image:: media/woodwork/IMG_20201027_115525.jpg
   :width: 600pt
   :alt: woodwork








